/**
 * Created by tcd-linux on 31.10.15.
 */
(function ($) {
    var jwm = function (methodOrOptions, instance) {

        var windowsAll = [];
        var defaults = {
            newWindowOffsetX: 25,
            newWindowOffsetY: 25,
            width: 300,
            height: 600,
            newWindowSelector: ".wm-window",
            newWindowHeader: ".wm-header",
            navBarSelector: ".wm-bottom",
            navBarItemSelector: ".wm-nav-item"

        };
        var windowObj;
        var navBar;
        var navItemObj;
        var opts;

        function setActive(obj) {
            $(windowsAll).each(function (index, value) {
                var current = $(value).css("z-index");
                $(value).css("z-index",current-1).addClass("active");
            });
            $(obj).css("z-index", windowsAll.length).removeClass("active");
        }

        function drag(obj, x, y) {
            var paddingLeft = parseInt($(obj).css('left').replace("px",""))
            var offset = x - paddingLeft;
            var moveX = x - offset;
            $(obj).css('left',moveX);
            //if (secondDragPass) {
            //    $(obj).css('left', x - $(obj).offset().left);
            //    secondDragPass = false
            //} else {
            //    var offset = x - $(obj);
            //    //console.log(Math.abs(offset) - $(obj).position().left);
            //    $(obj).css('left',$(obj).position().left - Math.abs(offset));
            //    secondDragPass = true;
            //}
            console.log({
                x: x,
                left: $(obj).css('left'),
                offset: offset,
                moveX: moveX
            });
            //
            //    console.log("move x to " + moveX);
            //    console.log("move y to " + y);
            //}
            //var offsets = $(obj).offset();
            //    var moveX = x - offsetLeft;
            //    var moveY = y - offsetTop;
            //console.log(x);

        }
        
        function addEvents(obj) {
            var isDragging = false;
            var offsetLeft;
            $(obj).click(function() {
                    setActive(obj);
            });
            var offsetTop;
            $(obj).find(opts.newWindowHeader).mousedown(function () {
                console.log("mousedown");
                
                offsetLeft = $(obj).offset().left;
                offsetTop = $(obj).offset().top;
                isDragging = true;
            }).mousemove(function (e) {
                //console.log("mousemove");
                if (isDragging) {
                    //console.log(e.pageX);
                    drag(obj, e.clientX, e.pageY);
                }
            }).mouseup(function () {
                console.log("mouseup");
                isDragging = false;

            }).mouseleave(function () {
                isDragging = false;
                console.log("mouseleave");
            });
            console.log($(opts.newWindowHeader));
            return obj;
        }

        function generateBottom() {

        }

        function generateArea() {
            windowObj = $(opts.newWindowSelector);
            navBar = $(opts.navBarSelector);
            navItemObj = $(opts.navBarSelector).find(opts.navBarItemSelector);
            $(navBar).empty();
            $(instance).empty();
            $(instance).append(navBar);


        }

        function generateWindow(options) {
            var xPosition, yPosition;

            var newWindowOptions = $.extend({}, options, defaults);

            xPosition = (windowsAll.length + 1) * newWindowOptions.newWindowOffsetX;
            console.log(xPosition);
            yPosition = (windowsAll.length + 1) * newWindowOptions.newWindowOffsetY;
            console.log(yPosition);
            var newWindow = $(windowObj).clone();
            $(newWindow).css({
                left: yPosition,
                top: xPosition,
                "z-index": windowsAll.length + 1
            });
            newWindow = addEvents(newWindow);
            windowsAll.push(newWindow);
            $(instance).append(newWindow);

            $(navBar).append(navItemObj.clone());
        }

        function init() {
            console.log("jwm initialization");
            opts = $.extend({}, defaults, opts);
            generateArea();
            generateBottom();
            addEvents();
            //exposing public methods
            return {
                addWindow: addWindow,
                windowsAll: windowObj
            }
        }

        function addWindow(options) {
            //add window
            console.log("opening window n = " + windowsAll.length);
            generateWindow(options);

        }

        return init();
    };
    $.fn.jwm = function (methodOrOptions) {

        return new jwm(methodOrOptions, this);
    }
})(jQuery);